var fs = require('fs');

var startAppender = function(fd, startPos) {

  var pos = startPos;
  return {
    append : function(buffer, callback) {
      var written = 0;
      var oldPos = pos;
      pos += buffer.length;
      (function tryWriting() {
        if (written < buffer.length) {
          fs.write(fd, buffer, written, buffer.length - written, oldPos
              + written, function(err, bytesWritten) {
            if (err) {
              callback(err);
              return;
            }
            console.log('wrote ' + bytesWritten + ' bytes');
            written += bytesWritten;
            tryWriting();
          });
        } else {
          callback(null);
        }
      })();
      fs.write(fd, buffer, 0, buffer.length, oldPos, callback);
    }
  };
};

fs.open('tmp2.txt', 'w', function(err, fd) {
  if (err) { throw err; }
  var appender = startAppender(fd, 0);
  var size = 1024 * 1024;

  for ( var int = 0; int < 3; int++) {
    var buffer = new Buffer(size);
    buffer.fill(int + '');

    appender.append(buffer, function(err) {
    });
  }
});
