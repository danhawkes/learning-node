var fs = require('fs');

// Getting file stats
fs.stat('stat.js', function(err, stats) {
	if (err) {
		console.log(err.message);
	} else {
		console.log(stats);
	}
});

// Writing and reading a file
var filename = 'tmp.txt';
fs.writeFile(filename, new Buffer('hello world'), function(err) {
	if (err) {
		console.log(err.message);
	} else {
		fs.readFile(filename, 'utf8', function(err, data) {
			if (err) {
				console.log(err.message);
			} else {
				console.log('read data from file: ' + data);
			}
		});
	}
});
