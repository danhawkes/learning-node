var http = require('http');
var exec = require('child_process').exec;

/*
 * Make an HTTP server that outputs plain text with 100 new-line separated unix
 * timestamps every second. (This doesn't make much sense - have implemented
 * server that gets 100 timestamps per request and writes them to the response.)
 */
var server = http.createServer();

server.listen(8080);

server.on('request', function(req, res) {
    res.writeHeader(200, {
        'Content-Type' : 'text/plain'
    });

    var count = 100;
    (function writeTime() {
        exec('date +%s', function(err, stdout, stderr) {
            if (err) { throw err; }
            if (count-- > 0) {
                res.write(stdout);
                writeTime();
            } else {
                res.end();
            }
        });
    })();
});
