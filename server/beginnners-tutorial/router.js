function route(handle, pathname, response) {
	console.log('About to route request for: ' + pathname + '.');
	if (typeof handle[pathname] === 'function') {
		console.log('Calling handler for ' + pathname);
		handle[pathname](response);
	} else {
		console.log('No handler found for ' + pathname);
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.write('404 Not Found');
		response.end();
	}
}


exports.route = route;