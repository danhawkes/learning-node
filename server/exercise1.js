var http = require('http');
var fs = require('fs');
var path = require('path');

/*
 * Make an HTTP server that serves files. The file path is provided in the URL
 * like this: http://localhost:4000/path/to/my
 */
var server = http.createServer();
var fileRoot = __dirname;

server.on('request', function(req, res) {

  var filename = path.join(process.cwd(), req.url);
  console.log('received request for ' + filename);

  fs.stat(filename, function(err, stats) {
    if (err) {
      writeError(res, 'failed to stat file');
      return;
    }
    if (stats.isFile()) {
      fs.readFile(filename, function(err, data) {
        if (err) {
          writeError(res, 'failed to read file');
        } else {
          writeData(res, data);
        }
      });
    } else {
      writeError(res, 'not a file');
    }
  });
});

var writeData = function(res, data) {
  res.writeHead(300, {
    'Content-Type' : 'text/plain'
  });
  res.end(data);
};

var writeError = function(res, err) {
  res.writeHead(300, {
    'Content-Type' : 'text/plain'
  });
  res.end(err);
};

server.listen(8080);
