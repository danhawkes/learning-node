/*
 * Create an uninitialized buffer with 100 bytes length and fill it with bytes
 * with values starting from 0 to 99. And then print its contents.
 */
var data1 = new Buffer(100);

for ( var i = 0; i < 100; i++) {
	data1[i] = i;
}

console.log(data1);

/*
 * Do what is asked on the previous exercise and then slice the buffer with
 * bytes ranging 40 to 60. And then print it.
 */
var data2 = data1.slice(40, 60);

console.log(data2);

/*
 * Do what is asked on exercise 1 and then copy bytes ranging 40 to 60 into a
 * new buffer. And then print it.
 */
var len = 20;
var data3 = new Buffer(len);

data1.copy(data3, 0, len, len + 20);

console.log(data3); 