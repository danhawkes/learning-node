var events = require('events');
var util = require('util');

/*
 * Build a pseudo-class named �Ticker� that emits a �tick� event every 1 second.
 */

var Ticker = function() {
	var _this = this;
	setInterval(function() {
		_this.emit('tick');
	}, 1000);
};

util.inherits(Ticker, events.EventEmitter);

/*
 * Build a script that instantiates one Ticker and bind to the �tick� event,
 * printing �TICK� every time it gets one.
 */
new Ticker().on('tick', function() {
	console.log('tick');
});