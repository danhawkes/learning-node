var events = require('events');
var util = require('util');

// Create an event emitter subclass
var MyEmitter = function(eventName) {
	this.eventName = eventName;
};
util.inherits(MyEmitter, events.EventEmitter);

// Add a method that emits events
MyEmitter.prototype.emitStuff = function() {
	this.emit(this.eventName, 'some arguments');
};

// Create instance of class
var eventName = 'something happened';
var myEmitter = new MyEmitter(eventName);

// Listen for events
myEmitter.on(eventName, function() {
	console.log('something happened!');
});

// Send an event
myEmitter.emitStuff();


