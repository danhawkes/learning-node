/*
 * Listen for an event once.
 */

var http = require('http');

// Create server
var server = http.createServer(function(request, response) {
	response.end();
}).listen(8080);

// Create request
var options = {
	host : 'localhost',
	port : 8080,
	path : '/'
};

// Send several requests then shutdown
http.get(options, function() {
	http.get(options, function() {
		http.get(options, function() {
			server.close();
		});
	});
});

// Listen for only the first request
server.once('request', function() {
	console.log('got request');
});
