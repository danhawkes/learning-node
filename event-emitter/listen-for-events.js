/**
 * Reads the contents of a file and listens for data and end events
 */
var fs = require('fs');
var readStream = fs.createReadStream('./event.js');

readStream.on('data', function(data) {
	console.log('got data ', data);
});

readStream.on('end', function() {
	console.log('file ended');
});